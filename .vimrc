"colorscheme jellybeans 
if &t_Co > 2 || has("gui_running")
    syntax on
endif
if &t_Co == 256
  colorscheme molokai 
  "set term=screen-256color
  "let g:molokai_original = 1
endif

" pathogen
execute pathogen#infect()


" turn on indent-guides
" autocmd VimEnter * IndentGuidesEnable
" let g:indent_guides_guide_size=1

set number
set nocompatible

set clipboard=unnamed

"statusline
"0->never, 1-> default, 2->always
set laststatus=2

" backspaces over everything in insert mode
set backspace=indent,eol,start

" word wrap
set nowrap

" Indent
set autoindent
set tabstop=2
set shiftwidth=2
"set softtabstop=4
set smartindent
"set expandtab 
syntax on

set textwidth=79
set formatoptions=qrn1
"set colorcolumn=80

" folding
" set foldmethod=indent
set nofoldenable


" Sidebar folder navigation
let NERDTreeShowLineNumbers=1
let NERDTreeShowBookmarks=1
let NERDTreeChDirMode=2
let NERDTreeWinSize=35
"let NERDTreeIgnore=['CVS']

set incsearch
set ignorecase
set smartcase
set visualbell
set noerrorbells
set hlsearch
set history=500

" scrolling
"set ruler
set scrolloff=10 " Scroll with 10 line buffer

" clear ANNOYING recent search highlight with space
:nnoremap <silent> <Space> :nohlsearch<Bar>:echo<CR>


" save files as root without prior sudo
cmap w!! w !sudo tee % >/dev/null

" json pretyify
cmap j! %!python -m json.tool

" Map ps to perl regex
cmap ps perldo s/

set nobackup
set noswapfile

"set list
"set listchars=tab:.\ ,trail:.,extends:#,nbsp:.

" line tracking
set numberwidth=5
set cursorline
set cursorcolumn 	" get rid of annoying cursor line. 
										" because FORGET you paul!!!

" turn off cursor blinking
set guicursor+=a:blinkon0
highlight Cursor guifg=#505050 guibg=#505050


nnoremap ; :

let mapleader = ','
noremap <Leader>, :NERDTreeToggle<cr>
map <Leader>t :tabnew<cr>
map <Leader>h :tabprevious<cr>
map <Leader>l :tabnext<cr>
map <Leader>w :tabclose<cr>
"
" cd to directory of current file
map <leader>cd :cd %:p:h<cr>
map <leader>F :NERDTreeFind<cr>
map <leader>R :source ~/.vimrc<cr>


" Move single lines up-down
nmap <c-up> ddkP
nmap <c-down> ddp


" Resize vertical windows
nmap + <c-w>+
nmap _ <c-w>-

" Resize horizontal windows
" mnap > <c-w>> "don't want this PAUL!
" nmap < <c-w>< "don't want this PAUL!

" Move multiple lines up-down
vmap <c-up> xkP`[V`]
vmap <c-down> xp`[V`]
"vmap <c-up> [egv
"vmap <c-down> ]egv

" autocompletion
imap <Leader><Tab> <C-X><C-O>

" js, css, & html tidy config (vim-jsbeautify)
let g:jsbeautify = {'indent_size': 2, 'indent_char': ' ', 'max_char': 5}
let g:htmlbeautify = {'indent_size': 2, 'indent_char': ' ', 'max_char': 80, 'brace_style': 'expand', 'unformatted': ['a', 'sub', 'sup', 'b', 'i', 'u']}
let g:csseautify = {'indent_size': 2, 'indent_char': ' ', 'max_char': 80, 'brace_style': 'expand'}

" js
au FileType javascript nmap <buffer> <leader>pt :call JsBeautify()<cr>
au FileType html nmap <buffer> <leader>pt :call HtmlBeautify()<cr>
au FileType css nmap <buffer> <leader>pt :call CSSBeautify()<cr>

filetype plugin on
au BufNewFile,BufRead *.ejs set filetype=html
